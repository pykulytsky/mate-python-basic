from tasks import all_tests

from inspect import getmembers, isfunction

for task in all_tests:
    for func in getmembers(task, isfunction):
        try:
            func[1]()
        except Exception as e:
            print(f"{func[0]} failed")
            print(f"Traceback: {str(e)}")
