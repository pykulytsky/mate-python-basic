from tasks.introduction import test_declare_variables
from tasks.main_concepts.hello_world import test_hello_world
from tasks.main_concepts.make_fruit_juice import test_make_fruit_juice
from tasks.main_concepts.single_line_comment import test_single_line_comment
from tasks.main_concepts.uncoment_code import test_uncomment_code

all_tests = [
    test_declare_variables,
    test_hello_world,
    test_make_fruit_juice,
    test_single_line_comment,
    test_uncomment_code
]
